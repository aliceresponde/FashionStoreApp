package com.example.alice.fashionstore;

import android.app.Application;
import android.content.res.Resources;
import android.util.Log;

import com.example.alice.fashionstore.DTO.Brand;
import com.example.alice.fashionstore.DTO.Type;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.exceptions.RealmError;

/**
 * Created by alice on 4/13/16.
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();




        //Config REALM
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(realmConfiguration);

        try {
            loadDB();
        }catch (RealmError error){
            Log.e("Realm Error", error.getMessage());
        }


    }

    public void loadDB() {
        Realm realm = Realm.getDefaultInstance();
        Resources res = getResources();
        String[] brands = res.getStringArray(R.array.brands_array);
        String[] types = res.getStringArray(R.array.clothing_types_array);

        if (realm.where(Type.class).count()==0){
            loadClothingTypes(realm, types);
        }

        if (realm.where(Brand.class).count()==0) {
            loadBrands(realm, brands);
        }

    }

    private void loadClothingTypes(Realm realm, String[] types) {
        for (String name : types ) {
            realm.beginTransaction();
            Type typeObject = realm.createObject(Type.class);
            typeObject.setName(name);
            realm.commitTransaction();
        }
    }

    private void loadBrands(Realm realm, String[] brands) {
        for (String name : brands ) {
            realm.beginTransaction();
            Brand brandObject = realm.createObject(Brand.class);
            brandObject.setName(name);
            realm.commitTransaction();
        }
    }


}
