package com.example.alice.fashionstore;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.alice.fashionstore.DTO.Brand;
import com.example.alice.fashionstore.DTO.Type;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {
    @Bind(R.id.tv_title)
    TextView tvTitle;
    @Bind(R.id.et_input)
    EditText etInput;
    @Bind(R.id.btn_search)
    Button btnSearch;
    @Bind(R.id.tv_brands)
    TextView tvBrands;
    @Bind(R.id.tv_clothing_types)
    TextView tvClothingTypes;
    @Bind(R.id.tv_result_query)
    TextView tvResultQuery;

    private Realm realm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        realm = Realm.getDefaultInstance();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @OnClick(R.id.btn_search)
    public void search(View view) {

        hideKeyboard();

        String str_input = etInput.getText().toString();


        String str_brands = " ";
        String str_clothing_type = " ";
        String str_query_result = str_input;

        if (str_input.length() == 0) {
            etInput.setError(getString(R.string.error_empty_search));
        } else {
            List<String> queryByWords = new ArrayList<>();
            queryByWords = Arrays.asList(str_input.split(" "));


            // Build the query looking at all Brands:
            RealmQuery<Brand> queryBrand = realm.where(Brand.class);
            RealmResults<Brand> resultBrand = queryBrand.findAll();
            Log.d("Input",str_input);
            Log.d("Brands",resultBrand.toString());

            Log.d("Input","==============BRAND=====================");
            for (Brand brand : resultBrand) {
                String brandName = brand.getName();
                Log.d("Input ",str_input);
                Log.d("brandN?", brandName);
                Log.d("Contains?", ""+str_input.toUpperCase().contains(brandName.toUpperCase()));
                if (str_input.toUpperCase().contains(brandName.toUpperCase())) {
                    Log.d("contain B", brandName);
                    str_brands+=  brandName + ",";
                    str_query_result = str_query_result.toUpperCase().replaceAll(brand.getName().toUpperCase(), "");
                }
            }


            // Build the query looking at all Brands:
            RealmQuery<Type> queryType = realm.where(Type.class);
            RealmResults<Type> resultType = queryType.findAll();
            Log.d("types", resultType.toString());
            Log.d("Input","===============TYPE===========================");

            for (Type type : resultType) {
                String typeName =type.getName();
                Log.d("Input ",str_input);
                Log.d("typeN?", typeName);
                Log.d("Contains?", ""+str_input.toUpperCase().contains(typeName.toUpperCase()));

                if (str_input.toUpperCase().contains(typeName.toUpperCase())) {
                    Log.d("contain T", typeName);
//                    str_clothing_type = str_clothing_type.concat(type.getName()).concat(" , ");
                    str_clothing_type +=  typeName + ",";
                    str_query_result = str_query_result.toUpperCase().replaceAll(type.getName().toUpperCase(), "");
                }
            }

            tvBrands.setText(str_brands.substring(0,str_brands.length()-1));
            tvClothingTypes.setText(str_clothing_type.substring(0, str_clothing_type.length()-1));
            tvResultQuery.setText(str_query_result);
        }
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
